=====
tryme
=====

.. list-table::
    :stub-columns: 1

    * - docs
      - |docs|
    * - tests
      - | |requires|
        | |codecov|
        |
    * - package
      - |version| |downloads| |wheel| |supported-versions| |supported-implementations|

.. |docs| image:: https://readthedocs.org/projects/python-tryme/badge/?style=flat
    :target: https://readthedocs.org/projects/python-tryme
    :alt: Documentation Status

.. |requires| image:: https://requires.io/github/ibuchanan/python-tryme/requirements.svg?branch=master
    :alt: Requirements Status
    :target: https://requires.io/github/ibuchanan/python-tryme/requirements/?branch=master

.. |codecov| image:: https://codecov.io/github/ibuchanan/python-tryme/coverage.svg?branch=master
    :alt: Coverage Status
    :target: https://codecov.io/github/ibuchanan/python-tryme
.. |version| image:: https://img.shields.io/pypi/v/tryme.svg?style=flat
    :alt: PyPI Package latest release
    :target: https://pypi.python.org/pypi/tryme

.. |downloads| image:: https://img.shields.io/pypi/dm/tryme.svg?style=flat
    :alt: PyPI Package monthly downloads
    :target: https://pypi.python.org/pypi/tryme

.. |wheel| image:: https://img.shields.io/pypi/wheel/tryme.svg?style=flat
    :alt: PyPI Wheel
    :target: https://pypi.python.org/pypi/tryme

.. |supported-versions| image:: https://img.shields.io/pypi/pyversions/tryme.svg?style=flat
    :alt: Supported versions
    :target: https://pypi.python.org/pypi/tryme

.. |supported-implementations| image:: https://img.shields.io/pypi/implementation/tryme.svg?style=flat
    :alt: Supported implementations
    :target: https://pypi.python.org/pypi/tryme

An example package. Replace this with a proper project description. Generated with https://github.com/ionelmc/cookiecutter-pylibrary

* Free software: BSD license

Installation
============

::

    pip install tryme

Documentation
=============

https://python-tryme.readthedocs.org/

Development
===========

To run the all tests run::

    tox
