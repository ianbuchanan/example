=======
trymore
=======

.. list-table::
    :stub-columns: 1

    * - docs
      - |docs|
    * - tests
      - | |travis| |appveyor| |requires|
        | |codecov|
        | |landscape| |scrutinizer| |codacy| |codeclimate|
    * - package
      - |version| |downloads| |wheel| |supported-versions| |supported-implementations|

.. |docs| image:: https://readthedocs.org/projects/python-trymore/badge/?style=flat
    :target: https://readthedocs.org/projects/python-trymore
    :alt: Documentation Status

.. |travis| image:: https://travis-ci.org/ibuchanan/python-trymore.svg?branch=master
    :alt: Travis-CI Build Status
    :target: https://travis-ci.org/ibuchanan/python-trymore

.. |appveyor| image:: https://ci.appveyor.com/api/projects/status/github/ibuchanan/python-trymore?branch=master&svg=true
    :alt: AppVeyor Build Status
    :target: https://ci.appveyor.com/project/ibuchanan/python-trymore

.. |requires| image:: https://requires.io/github/ibuchanan/python-trymore/requirements.svg?branch=master
    :alt: Requirements Status
    :target: https://requires.io/github/ibuchanan/python-trymore/requirements/?branch=master

.. |codecov| image:: https://codecov.io/github/ibuchanan/python-trymore/coverage.svg?branch=master
    :alt: Coverage Status
    :target: https://codecov.io/github/ibuchanan/python-trymore

.. |landscape| image:: https://landscape.io/github/ibuchanan/python-trymore/master/landscape.svg?style=flat
    :target: https://landscape.io/github/ibuchanan/python-trymore/master
    :alt: Code Quality Status

.. |codacy| image:: https://img.shields.io/codacy/REPLACE_WITH_PROJECT_ID.svg?style=flat
    :target: https://www.codacy.com/app/ibuchanan/python-trymore
    :alt: Codacy Code Quality Status

.. |codeclimate| image:: https://codeclimate.com/github/ibuchanan/python-trymore/badges/gpa.svg
   :target: https://codeclimate.com/github/ibuchanan/python-trymore
   :alt: CodeClimate Quality Status
.. |version| image:: https://img.shields.io/pypi/v/trymore.svg?style=flat
    :alt: PyPI Package latest release
    :target: https://pypi.python.org/pypi/trymore

.. |downloads| image:: https://img.shields.io/pypi/dm/trymore.svg?style=flat
    :alt: PyPI Package monthly downloads
    :target: https://pypi.python.org/pypi/trymore

.. |wheel| image:: https://img.shields.io/pypi/wheel/trymore.svg?style=flat
    :alt: PyPI Wheel
    :target: https://pypi.python.org/pypi/trymore

.. |supported-versions| image:: https://img.shields.io/pypi/pyversions/trymore.svg?style=flat
    :alt: Supported versions
    :target: https://pypi.python.org/pypi/trymore

.. |supported-implementations| image:: https://img.shields.io/pypi/implementation/trymore.svg?style=flat
    :alt: Supported implementations
    :target: https://pypi.python.org/pypi/trymore

.. |scrutinizer| image:: https://img.shields.io/scrutinizer/g/ibuchanan/python-trymore/master.svg?style=flat
    :alt: Scrutinizer Status
    :target: https://scrutinizer-ci.com/g/ibuchanan/python-trymore/

An example package. Replace this with a proper project description. Generated with https://github.com/ionelmc/cookiecutter-pylibrary

* Free software: BSD license

Installation
============

::

    pip install trymore

Documentation
=============

https://python-trymore.readthedocs.org/

Development
===========

To run the all tests run::

    tox
